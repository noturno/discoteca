import { bearer } from "@elysiajs/bearer";
import { cron } from "@elysiajs/cron";
import { html } from "@elysiajs/html";
import { jwt } from "@elysiajs/jwt";
import { staticPlugin } from "@elysiajs/static";
import { Elysia } from "elysia";
const workerURL = new URL("./worker-bot.ts", import.meta.url).href;
const worker = new Worker(workerURL);

worker.onmessage = (event) => {
  console.log(event.data);
};

const cegaliURL = new URL("./workers/bots/cegali.ts", import.meta.url).href;
const cegali = new Worker(cegaliURL);

cegali.onmessage = (event) => {
  console.log(event.data);
};

const app = new Elysia()
  .use(staticPlugin())
  .use(
    jwt({
      secret: process.env.SECRET || "secret",
    }),
  )
  .use(bearer())
  .use(html())
  .use(
    cron({
      name: "work-thred-test",
      pattern: "*/20 * * * * *",
      paused: true,
      run() {
        worker.postMessage("hello");
      },
    }),
  )
  .use(
    cron({
      name: "work-thred-cegali",
      pattern: "*/20 * * * * *",
      run() {
        cegali.postMessage("hello");
        cegali.postMessage(
          "Estou querendo dar as " +
            new Date().getHours() +
            " horas" +
            new Date().getMinutes() +
            " minutos" +
            new Date().getSeconds() +
            " segundos",
        );
      },
    }),
  )
  .get("/:username/env", (ctx) => {
    return { ...ctx };
  })
  .get("/", () => "Hello Elysia")
  .listen(3000, () =>
    console.log("Server is running on port 3000 http://localhost:3000"),
  );
