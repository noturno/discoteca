import windows from "winston";

const logger = windows.createLogger({
  transports: [
    new windows.transports.Console({
      format: windows.format.combine(
        windows.format.colorize({ all: true }),
        windows.format.timestamp(),
        windows.format.printf((info) => {
          return `${info.timestamp} ${info.level}: ${info.message}`;
        })
      ),
    }),
  ],
});

export default logger;
