import { DataPayload, getBotWrapper } from "./utils";

const cegaliCon = getBotWrapper(
  "https://discord.com/api/webhooks/1140871501520842782",
  "cegali"
);

self.onmessage = (event: MessageEvent) => {
  cegaliCon.post<unknown, null, DataPayload>(
    "https://discord.com/api/webhooks/1140871501520842782/og5NKlB5yhyW-5pSh6fD1Gfl9LCtqy24GAZSYTcy49tFxlYUA3INDscCzn0nR0Yh1zlt",
    {
      content: event.data,
      username: "MC Pipokinha",
      avatar_url:
        "https://assets.codepen.io/10717752/MC-Pipokinha.webp?format=auto",
    }
  );
};
