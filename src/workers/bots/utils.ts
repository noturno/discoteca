import axios from "axios";
import windows from "winston";

export type DataPayload = {
  content: string;
  username?: string;
  avatar_url?: string;
  tts?: boolean;
  file?: any;
  embeds?: any;
  payload_json?: string;
  allowed_mentions?: any;
  components?: any;

  // Embed
  title?: string;
  type?: string;
  description?: string;
  url?: string;
  timestamp?: string;
  color?: number;
  footer?: any;
  image?: any;
  thumbnail?: any;
  video?: any;
  provider?: any;
  author?: any;
  fields?: any;

  // Embed Footer
  footer_text?: string;
  footer_icon_url?: string;
  footer_proxy_icon_url?: string;
};

const logger = windows.createLogger({
  transports: [
    new windows.transports.Console({
      format: windows.format.combine(
        windows.format.colorize(),
        windows.format.timestamp(),
        windows.format.printf((info) => {
          return `${info.timestamp} ${info.level}: ${info.message}`;
        })
      ),
    }),
  ],
});

export const getBotWrapper = (webhookUrl: string, workerName: string) => {
  const instance = axios.create({
    url: webhookUrl,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    transformRequest: [
      (data, headers) => {
        headers["X-Worker-Name"] = workerName;
        logger.log(
          "info",
          `Sending data to discord with worker name ${headers["X-Worker-Name"]}`
        );

        logger.log("info", "Sending data to discord");
        return JSON.stringify(data);
      },
    ],

    transformResponse: [
      (data) => {
        logger.log("info", "Received data from discord");
        return data;
      },
    ],
    xsrfCookieName: process.env.COOKIE_NAME + "_" + workerName,
    xsrfHeaderName: process.env.COOKIE_HEADER + "_" + workerName,
  });

  return instance;
};
